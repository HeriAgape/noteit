package co.heri.noteit;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;


public class SingleNoteActivity extends ActionBarActivity {

    String sURL = "http://www.heri.co/android/api/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // from the intent
        setContentView(R.layout.activity_single_note);
        NoteItem selectedNote = (NoteItem) getIntent().getSerializableExtra("selectedNote");

        this.setTitle(selectedNote.getTitle());

        TextView title = (TextView) findViewById(R.id.noteTitle);

        TextView content = (TextView) findViewById(R.id.postFull);

        title.setText(selectedNote.getTitle().toString());
        content.setText(selectedNote.getContent().toString());

        ImageView noteImage = (ImageView) findViewById(R.id.noteImage);

        String imgLink = sURL + selectedNote.getImage().toString();
        Log.i("message", imgLink);

        new DownloadImageTask(noteImage)
                .execute(imgLink);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_single_note, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
