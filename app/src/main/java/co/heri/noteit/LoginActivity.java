package co.heri.noteit;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class LoginActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText  userName = (EditText) findViewById(R.id.emailText);

        final EditText  userPassword = (EditText) findViewById(R.id.passText);

        Button loginBtn = (Button) findViewById(R.id.loginBtn);

        TextView registerText = (TextView) findViewById(R.id.registerLink);

        registerText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(LoginActivity.this, RegisterActivity.class);

                LoginActivity.this.startActivity(myIntent);

                LoginActivity.this.finish();
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                String sURL = "http://www.heri.co/android/api/login.php";

                new LoginTask().execute(sURL, userName.getText().toString(), userPassword.getText().toString());



            }
        });
    }

    private long lastBackPressTime = 0;
    private Toast toast;

    @Override
    public void onBackPressed() {
        if (this.lastBackPressTime < System.currentTimeMillis() - 4000) {
            toast = Toast.makeText(getApplicationContext(), "Press back again to close this app", Toast.LENGTH_LONG);
            toast.show();
            this.lastBackPressTime = System.currentTimeMillis();
        } else {
            if (toast != null) {
                toast.cancel();
            }
            super.onBackPressed();
        }
    }



    class LoginTask extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog myPd_ring;

        @Override
        protected void onPreExecute() {

            myPd_ring  = new ProgressDialog (LoginActivity.this);
            myPd_ring.setMessage("Please wait");
            myPd_ring.show();

        }


        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject a = null;

            try {
                //        String sURL = "http://10.55.15.65/api/index.php";

                String sURL = params[0];
                String username = params[1];
                String password = params[2];

                String parameters = "?username=" + username + "&password=" + password;

                // Connect to the URL using java's native library

                URL url = new URL(sURL+parameters);
                HttpURLConnection request = (HttpURLConnection) url.openConnection();
                request.setRequestMethod("GET");
                request.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                request.setRequestProperty("charset", "utf-8");

                // Add your data
                request.connect();

                JSONParser parser = new JSONParser();
                a  = (JSONObject) parser.parse(new InputStreamReader((InputStream) request.getContent()));


            } catch (IOException | ParseException e) {
                e.printStackTrace();
            }

            return a;

        }
        protected void onPostExecute(JSONObject data) {
            // do further things with JSONObject as this runs on UI thread.
            Log.i("message", "OnPostExecute was a success");

                JSONObject details = data;

                int success = Integer.parseInt(details.get("success").toString());

                if (success == 1){
                    String user_id = details.get("user_id").toString();
                    Log.i("message", "" + success + " - " + user_id);

                    Intent myIntent = new Intent(LoginActivity.this, MainActivity.class);

                    myIntent.putExtra("user_id", user_id);

                    LoginActivity.this.startActivity(myIntent);

                    LoginActivity.this.finish();

                } else {
                    System.out.println(success);
                    Log.i("message", "Invalid logins");

                    TextView errotext = (TextView) findViewById(R.id.errMessage);

                    errotext.setText(details.get("reason").toString());
                }
            myPd_ring.dismiss();
            }

        }
    }

