package co.heri.noteit;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by heri on 7/2/15.
 */
public class NoteItem implements Serializable {

    private String title;
    private String content;
    private Date time;
    String image;



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public NoteItem(String title, String content, String image) {
        this.title = title;
        this.content = content;
        //this.time = time;
        this.image = image;
    }

    public String getShortContent(){
        return this.content.substring(0,130) + " [...]";
    }

}
