package co.heri.noteit;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    String sURL = "http://www.heri.co/android/api/";

    private Toolbar toolbar;

    private List<NoteItem> myNotes = new ArrayList<NoteItem>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        this.setTitle("");
        toolbar = (Toolbar) findViewById(R.id.tool_bar); // Attaching the layout to the toolbar object
        setSupportActionBar(toolbar);                   // Setting toolbar as the ActionBar with setSupportActionBar() call
        toolbar.setLogo(R.mipmap.drawing);


        String user_id = getIntent().getStringExtra("user_id").toString();

        String bURL = sURL + "?user=" + user_id;

        new Noteloader().execute(bURL); // loads the webservice data
        Log.i("message", "Background Process went on well");
        populateListView(); // the single list view
        Log.i("message", "Populated the list");
        registerClickCallback(); // registers events to each list item
        Log.i("message", "Events are registred");



    }

    private long lastBackPressTime = 0;
    private Toast toast;

    @Override
    public void onBackPressed() {
        if (this.lastBackPressTime < System.currentTimeMillis() - 4000) {
            toast = Toast.makeText(getApplicationContext(), "Press back again to close this app", Toast.LENGTH_LONG);
            toast.show();
            this.lastBackPressTime = System.currentTimeMillis();
        } else {
            if (toast != null) {
                toast.cancel();
            }
            super.onBackPressed();
        }
    }



    public void populateListView(){
        ArrayAdapter<NoteItem> adapter = new MyListAdapter();

        ListView list = (ListView) findViewById(R.id.noteList);

        list.setAdapter(adapter);
    }

        // Very important for setting the list view item
    private class MyListAdapter extends ArrayAdapter<NoteItem>{

        public MyListAdapter() {
            super(MainActivity.this, R.layout.rowlayout, myNotes);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // makes sure we have a view to work with
            View itemView = convertView;
            if (itemView == null) {
                itemView = getLayoutInflater().inflate(R.layout.rowlayout, parent, false);
            }
            // populates the list
            NoteItem myNote = myNotes.get(position);

            // fill the view -- VERY IMPORTANT

            ImageView noteImage = (ImageView) itemView.findViewById(R.id.postImage);
            // this download and displayes the image but in the backgound thread
            String imgLink = sURL + myNote.getImage().toString();
            Log.i("message", imgLink);

            new DownloadImageTask(noteImage)
                    .execute(imgLink);

            TextView noteTitle = (TextView) itemView.findViewById(R.id.postTitle);
            noteTitle.setText(myNote.getTitle().toString());

            TextView noteContent = (TextView) itemView.findViewById(R.id.postShortContent);
            noteContent.setText(myNote.getShortContent().toString());

            return itemView;
           // return super.getView(position, convertView, parent);
        }
    }


    class Noteloader extends AsyncTask<String, Void, JSONArray> {

        ProgressDialog myPd_ring;

        @Override
        protected void onPreExecute() {

            myPd_ring  = new ProgressDialog (MainActivity.this);
            myPd_ring.setMessage("Please wait");
            myPd_ring.show();

        }

        @Override
        protected JSONArray doInBackground(String... params) {
            JSONArray a = null;
            try {
                //        String sURL = "http://10.55.15.65/api/index.php";

                String aURL = params[0];

                // Connect to the URL using java's native library
                URL url = new URL(aURL);
                HttpURLConnection request = (HttpURLConnection) url.openConnection();
                request.setRequestMethod("GET");

                request.connect();

                JSONParser parser = new JSONParser();
                a  = (JSONArray) parser.parse(new InputStreamReader((InputStream) request.getContent()));


            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }


            return a;

        }
        protected void onPostExecute(JSONArray data) {
            // do further things with JSONObject as this runs on UI thread.
            Log.i("message", "OnPostExecute was a success");
            for (Object o : data) {
                JSONObject note = (JSONObject) o;

                String title = (String) note.get("title");
                System.out.println(title);

                String content = (String) note.get("content");
                System.out.println(content);

                String image = (String) note.get("image");
                System.out.println(image);

                NoteItem noteReceived = new NoteItem(title, content, image);

                myNotes.add(noteReceived);

            /*JSONArray cars = (JSONArray) person.get("cars");

            for (Object c : cars)
            {
                System.out.println(c+"");
            }*/

                populateListView(); // the single list view

                registerClickCallback(); // registers events to each list item


            }
            myPd_ring.dismiss();
        }
    }




    public void registerClickCallback(){
        ListView list = (ListView) findViewById(R.id.noteList);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View viewClicked, int position, long id) {

                NoteItem selectedNote = myNotes.get(position);

                /*String message = "You cliked position " + position + " note with title " + selectedNote.getTitle();
                Toast.makeText(MainActivity.this,message, Toast.LENGTH_LONG).show();*/

                Intent myIntent = new Intent(MainActivity.this, SingleNoteActivity.class);

                myIntent.putExtra("selectedNote", selectedNote);

                MainActivity.this.startActivity(myIntent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {

            Intent myIntent = new Intent(MainActivity.this, LoginActivity.class);

            MainActivity.this.startActivity(myIntent);
            MainActivity.this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

