package co.heri.noteit;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;


public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        final EditText fullName = (EditText) findViewById(R.id.fullName_tv);
        final EditText email = (EditText) findViewById(R.id.email_tv);
        final EditText username = (EditText) findViewById(R.id.username_tv);
        final EditText password1 = (EditText) findViewById(R.id.password1_tv);
        final EditText password2 = (EditText) findViewById(R.id.password2_tv);

        Button registerBtn = (Button) findViewById(R.id.registerBtn);

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean fieldsOK=checkEmptyFields(new EditText[]{fullName, email, username, password1, password2});

                if (fieldsOK) {
                    if (!password1.getText().toString().equals(password2.getText().toString())) {

                        Toast.makeText(getApplicationContext(), "The two passwords don't match!",
                                Toast.LENGTH_LONG).show();
                    } else {
                        String sURL = "http://www.heri.co/android/api/register.php";

                        try {
                            new RegisterTask().execute(sURL,
                                    URLEncoder.encode(fullName.getText().toString(), "UTF-8"),
                                    URLEncoder.encode(email.getText().toString(), "UTF-8"),
                                    URLEncoder.encode(username.getText().toString(), "UTF-8"),
                                    URLEncoder.encode(password1.getText().toString(), "UTF-8")
                            );
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                    }  else {
                    Toast.makeText(getApplicationContext(), "You need to provide all the information required!",
                            Toast.LENGTH_LONG).show();
                }
            }
        });





        TextView loginLink = (TextView) findViewById(R.id.loginLink);

        loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(RegisterActivity.this, LoginActivity.class);
                RegisterActivity.this.startActivity(myIntent);
                RegisterActivity.this.finish();
            }
        });

    }

    private long lastBackPressTime = 0;
    private Toast toast;

    @Override
    public void onBackPressed() {
        if (this.lastBackPressTime < System.currentTimeMillis() - 4000) {
            toast = Toast.makeText(getApplicationContext(), "Press back again to close this app", Toast.LENGTH_LONG);
            toast.show();
            this.lastBackPressTime = System.currentTimeMillis();
        } else {
            if (toast != null) {
                toast.cancel();
            }
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // this checks if any field is empty
    private boolean checkEmptyFields(EditText[] fields){
        for(int i=0; i<fields.length; i++){
            EditText currentField=fields[i];
            if(currentField.getText().toString().length()<=0){
                return false;
            }
        }
        return true;
    }

    class RegisterTask extends AsyncTask<String, Void, JSONObject> {

        ProgressDialog myPd_ring;

        @Override
        protected void onPreExecute() {

            myPd_ring  = new ProgressDialog (RegisterActivity.this);
            myPd_ring.setMessage("Please wait");
            myPd_ring.show();

        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject a = null;
            try {
                //        String sURL = "http://10.55.15.65/api/index.php";

                String sURL = params[0];
                String fullname = params[1];
                String email = params[2];
                String username = params[3];
                String password = params[4];


                String parameters = "?" +
                        "fullname=" + fullname +
                        "&email=" + email +
                        "&username=" + username +
                        "&password=" + password;

                // Connect to the URL using java's native library

                URL url = new URL(sURL+parameters);
                HttpURLConnection request = (HttpURLConnection) url.openConnection();
                request.setRequestMethod("GET");
                request.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                request.setRequestProperty("charset", "utf-8");

                // Add your data
                request.connect();

                JSONParser parser = new JSONParser();
                a  = (JSONObject) parser.parse(new InputStreamReader((InputStream) request.getContent()));


            } catch (IOException | ParseException e) {
                e.printStackTrace();
            }


            return a;

        }
        protected void onPostExecute(JSONObject data) {
            // do further things with JSONObject as this runs on UI thread.
            Log.i("message", "OnPostExecute was a success");

            JSONObject details = data;

            int success = Integer.parseInt(details.get("success").toString());

            if (success == 1){
                String user_id = details.get("user_id").toString();
                Log.i("message", "" + success + " - " + user_id);

                Intent myIntent = new Intent(RegisterActivity.this, MainActivity.class);

                myIntent.putExtra("user_id", user_id);

                RegisterActivity.this.startActivity(myIntent);

                RegisterActivity.this.finish();

            } else {
                System.out.println(success);
                Log.i("message", "Invalid logins");

                TextView errotext = (TextView) findViewById(R.id.errMessage);

                errotext.setText(details.get("reason").toString());
            }



            myPd_ring.dismiss();


        }

    }
}
